const express = require('express')
const http = require('http')

const app = express()
const server = http.createServer(app)

// Configura socket io
const io = require('socket.io')(server);


const PORT = 8080

server.listen(PORT, () => {
  console.log(`Server ready at port: ${PORT}` )

  io.on('connection', socket => {
    console.log('Socket conectado: New Connection!', socket.id);

    io.on('req:microservice:findAll',  ({}) => {
      console.log("LLamado!!");
    })

  })
})