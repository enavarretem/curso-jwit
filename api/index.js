const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueFindAll } = require('../microservice/src/Adapters/index')


async function Create() {
  try {

    const data = {
      name: 'Erik',
      age: '1000',
      color: 'rojo'
    }

    const job = await queueCreate.add(data)
    const result = await job.finished()
    
    console.log({result});
    
  } catch (error) {
    console.log('Test Create', error);
  }
}

async function Update() {
  try {
    const data = {
      name: 'ErikUpdated',
      age: '12',
      color: 'verde',
      id: '1'
    }
    const job = await queueUpdate.add(data)
    const result = await job.finished()
    
    console.log(result);
    
  } catch (error) {
    console.log('Test Update', error);
  }
}


async function Delete() {
  try {
    const data = {
      name: 'Erik'
    }
    const job = await queueDelete.add(data)
    const result = await job.finished()
    
    console.log(result);
    
  } catch (error) {
    console.log('Test Delete', error);
  }
}

async function FindOne() {
  try {
    const data = {
      name: 'ErikUpdated'
    }
    const job = await queueFindOne.add(data)
    const result = await job.finished()
    
    console.log(result);
    
  } catch (error) {
    console.log('Error en FindOne', error);
  }
}

async function FindAll() {
  try {
    const job = await queueFindAll.add({})
    const result = await job.finished()
    
    console.log(result);
    
  } catch (error) {
    console.log('Error en FindAll', error);
  }
}

async function main(){
  // Create();
  FindAll();
  // Update();
  // FindOne()
  // Delete();
}

main();

