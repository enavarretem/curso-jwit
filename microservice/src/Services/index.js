const { ExistUser, FindUser, create, deleteById, update, findOne, findAll } = require("../Controllers");

async function Create({ name, age, color }) {
  try {
    return await create({ name, age, color });
  } catch (error) {

    console.log({ step: 'services Create', error: error });
    return { statusCode: 500, message: error }
  }
}

async function Delete({ name }) {
  try {
    return await deleteById({where:{name}});
  } catch (error) {

    console.log({ step: 'services Delete', error: error });
    return { statusCode: 500, message: error }
  }
}

async function Update({ name, age, color, id }) {
  try {
    return await update({ name, age, color, id });
  } catch (error) {

    console.log({ step: 'services Update', error: error });
    return { statusCode: 500, message: error }
  }
}

async function FindOne({ name }) {
  try {
    return await findOne({ where:{name} });
  } catch (error) {

    console.log({ step: 'services FindOne', error: error });
    return { statusCode: 500, message: error }
  }
}

async function FindAll() {
  try {
    return await findAll();
  } catch (error) {
    console.log({ step: 'services FindAll', error: error });
    return { statusCode: 500, message: error }
  }
}

module.exports = {Create, Delete, Update, FindOne, FindAll}