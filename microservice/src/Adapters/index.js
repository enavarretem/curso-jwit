const bull = require('bull');
const { redis } = require('../settings');

console.log("Bull...");

const queueCreate = bull("curso:create", { redis });
const queueDelete = bull("curso:delete", { redis });
const queueUpdate = bull("curso:update", { redis });
const queueFindOne = bull("curso:findone", { redis });
const queueFindAll = bull("curso:findall", { redis });

module.exports = {queueCreate, queueDelete, queueUpdate, queueFindOne, queueFindAll}
  