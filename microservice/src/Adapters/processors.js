const { queueFindAll, queueFindOne, queueDelete, queueCreate, queueUpdate } = require('.');
const { FindAll, FindOne, Delete, Create, Update } = require('../Services');
const { internalError } = require('../settings');

const processorFindAll = async (job, done) => {
    try {      
      const { statusCode, data, message } = await FindAll();
      done(null, { statusCode, data, message })
    } catch (error) {
      console.log({ step: "adapter queueFindAll", error: error });
      done(null, { statusCode: 500, message: internalError })
    }

}

const processorFindOne = async (job, done) => {
  try {      
    const {name} = job.data;
    const { statusCode, data, message } = await FindOne({name});
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: "adapter queueFindOne", error: error });
    done(null, { statusCode: 500, message: internalError })
  }

}

const processorDelete = async (job, done) => {
  try {      
    const {name} = job.data;
    const { statusCode, data, message } = await Delete({name});
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: "adapter queueDelete", error: error });
    done(null, { statusCode: 500, message: internalError })
  }

}

const processorCreate = async (job, done) => {
  try {      
    const {name, age, color} = job.data;
    const { statusCode, data, message } = await Create({name, age, color});
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: "adapter queueCreate", error: error });
    done(null, { statusCode: 500, message: internalError })
  }

}

const processorUpdate = async (job, done) => {
  try {          
    const {name, age, color, id} = job.data;
    const { statusCode, data, message } = await Update({name, age, color, id});
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: "adapter queueUpdate", error: error });
    done(null, { statusCode: 500, message: internalError })
  }

}

async function run(){
  try {
    console.log("Iniciando endpoints");
    queueFindAll.process(processorFindAll)
    queueFindOne.process(processorFindOne)
    queueDelete.process(processorDelete)
    queueCreate.process(processorCreate)
    queueUpdate.process(processorUpdate)
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  processorCreate, processorDelete, processorFindAll, processorFindOne, processorUpdate, run
}