const { Model } = require('../Models')

const create = async ({name, age, color}) =>{
  try {
    const newObj = await Model.create({name, age, color}, {fields: ['name', 'age', 'color']})
    return {statusCode: 200, data: newObj.toJSON()}
  } catch (error) {
    console.log({step: 'controller Create', error: error.toString()});
    return {statusCode: 400, message: error.toString()}
  }
}

const deleteById = async ({where = {}}) =>{
  try {
    await Model.destroy({where})
    return {statusCode: 200, data: 'OK'}
    
  } catch (error) {
    console.log({step: 'controller delete', error: error.toString()});
    return {statusCode: 400, message: error.toString()}
  }
}

const update = async ({name, age, color, id}) =>{
  try {
    const dataUpdated = await Model.update(
      {name, age, color}, {where: {id}}
    )
    return {statusCode: 200, data: dataUpdated[0]}
  } catch (error) {
    console.log({step: 'controller update', error: error.toString()});
    return {statusCode: 400, message: error.toString()}
  }
}

const findOne = async ({where = {}}) =>{
  try {
    const obj = await Model.findOne({where})
    return {statusCode: 200, data: obj.toJSON()}

  } catch (error) {
    console.log({step: 'controller findOne', error: error.toString()});
    return {statusCode: 400, message: error.toString()}
  }
}

const findAll = async () =>{
  try {
    const data = await Model.findAll()
    return {statusCode: 200, data}
    
  } catch (error) {
    console.log({step: 'controller FindAll', error: error.toString()});
    return {statusCode: 200, message: error.toString()}
  }
}

module.exports = { create, deleteById, update, findOne, findAll }