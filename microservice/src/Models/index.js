const { DataTypes } = require("sequelize")
const { sequelize } = require("../settings")

const Model = sequelize.define('curso',
  {
    name: {type: DataTypes.STRING},
    age: {type: DataTypes.BIGINT},
    color: {type: DataTypes.STRING}
  })

const SyncDB =async () => {

  try {

    await Model.sync()
    return {statusCode: 200, data: 'OK'}
  } catch (error) {
    console.log('Error al inicializar DB');
    throw new Error(error.toString())
  }

}

module.exports = { SyncDB, Model }