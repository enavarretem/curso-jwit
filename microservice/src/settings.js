const dotenv = require('dotenv')
const {Sequelize} = require('sequelize')

dotenv.config();

const redis = {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT
}

const internalError = "No podemos procesar la petición!"

const dbConnection = {
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  database: process.env.POSTGRES_DB,
  username: process.env.POSTGRES_USERNAME,
  password: process.env.POSTGRES_PASSWORD,
  dialect: 'postgres',
  logging: false
}
const sequelize = new Sequelize(dbConnection)

module.exports = { redis, sequelize, internalError }