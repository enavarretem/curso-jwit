const micro = require('./src')

const main = async () => {
  try {
    console.log("Iniciando db...");
    await micro.SyncDB()

    micro.run()

  } catch (error) {
    console.log(error);
    return { statusCode: 500, message: error.toString() }
  }
}


main()