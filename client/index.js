const {io} = require('socket.io-client')

const socket = io("http://localhost:8080")

const main = () => {
  try {
    
    setTimeout(() => {
      console.log('Socket?');
      console.log(socket.id);
    }, 500);

    socket.on("res:microservice:findAll", ({statusCode, data, message}) => {
      console.log("res:microservice:findAll", {statusCode, data, message});
    })

    setInterval(() => socket.emit("req:microservice:findAll", ({})), 5000);

  } catch (error) {
    console.log("Client: ", error);
  }
}

main();